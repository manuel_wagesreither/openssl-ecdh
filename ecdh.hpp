#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <openssl/evp.h>
#include <openssl/ec.h>
#include <openssl/err.h>
#include <openssl/x509.h>


static void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}


class ParameterGenerationCtx {
public:
    ParameterGenerationCtx() {
        /* Create the context for parameter generation */
        if(NULL == (m_pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL))) handleErrors();

        /* Initialise the parameter generation */
        if(1 != EVP_PKEY_paramgen_init(m_pctx)) handleErrors();

        /* We're going to use the ANSI X9.62 Prime 256v1 curve */
        if(1 != EVP_PKEY_CTX_set_ec_paramgen_curve_nid(m_pctx, NID_X9_62_prime256v1)) handleErrors();
    }

    ~ParameterGenerationCtx() {
        EVP_PKEY_CTX_free(m_pctx);
    }

    EVP_PKEY_CTX * get() {
        return m_pctx;
    }

private:
    EVP_PKEY_CTX *m_pctx;
};


class ParameterObjParams {
public:
    ParameterObjParams() {}
    ParameterObjParams( EVP_PKEY_CTX *pctx ) {
        /* Create the parameter object params */
        if (!EVP_PKEY_paramgen(pctx, &m_params)) handleErrors();
    }

    ~ParameterObjParams() {
        EVP_PKEY_free(m_params);
    }

    EVP_PKEY * get() {
        return m_params;
    }

private:
    EVP_PKEY *m_params = NULL;
};


class KeyGenerationCtx {
public:
    KeyGenerationCtx() {}
    KeyGenerationCtx( EVP_PKEY *params ) {
        /* Create the context for the key generation */
        if(NULL == (m_kctx = EVP_PKEY_CTX_new(params, NULL))) handleErrors();

        /* Generate the key */
        if(1 != EVP_PKEY_keygen_init(m_kctx)) handleErrors();
    }

    ~KeyGenerationCtx() {
        EVP_PKEY_CTX_free(m_kctx);
    }

    EVP_PKEY_CTX * get() {
        return m_kctx;
    }

private:
    EVP_PKEY_CTX *m_kctx;
};


class Privkey {
public:
    Privkey() {}
    Privkey( EVP_PKEY_CTX *kctx ) {
        if (1 != EVP_PKEY_keygen(kctx, &m_pkey)) handleErrors();
    }

    ~Privkey() {
        EVP_PKEY_free(m_pkey);
    }

    EVP_PKEY * get() {
        return m_pkey;
    }

    std::vector<unsigned char> export_pubkey() {
        ptrdiff_t length;
        if(0 > (length = i2d_PUBKEY(m_pkey, NULL))) handleErrors();

        unsigned char buffer[length];
        unsigned char *ptr = &buffer[0];

        // int i2d_PUBKEY(EVP_PKEY *a, unsigned char **pp);
        length = i2d_PUBKEY(m_pkey, &ptr);
        if( length != (ptr - &buffer[0])) handleErrors();

        return std::vector<unsigned char>( &buffer[0], &buffer[0] + sizeof(buffer) );
    }

private:
    EVP_PKEY *m_pkey = NULL;
};


class SecretDerivationCtx {
public:
    SecretDerivationCtx() {}
    SecretDerivationCtx( EVP_PKEY *pkey) {
        /* Create the context for the shared secret derivation */
        if(NULL == (m_ctx = EVP_PKEY_CTX_new(pkey, NULL))) handleErrors();

        /* Initialise */
        if(1 != EVP_PKEY_derive_init(m_ctx)) handleErrors();
    }

    ~SecretDerivationCtx() {
        EVP_PKEY_CTX_free(m_ctx);
    }

    EVP_PKEY_CTX * get() {
        return m_ctx;
    }

private:
    EVP_PKEY_CTX *m_ctx;
};


class ecdh_key {
public:
    ecdh_key() :
        m_params( m_pctx.get() ),
        m_kctx( m_params.get() ),
        m_pkey( m_kctx.get() ),
        m_ctx( m_pkey.get() ) {}

    ~ecdh_key() {}

#ifdef EXPOSE_INTERNALS
    EVP_PKEY * get_privkey() {
        return m_pkey.get();
    }
#endif

    std::vector<unsigned char> get_pubkey() {
        return m_pkey.export_pubkey();
    }

    std::vector<unsigned char> derive_secret( EVP_PKEY * peerkey ) {
        size_t secret_len;
        unsigned char * secret;
        EVP_PKEY_CTX *ctx = m_ctx.get();

        /* Provide the peer public key */
        if(1 != EVP_PKEY_derive_set_peer(ctx, peerkey)) handleErrors();

        /* Determine buffer length for shared secret */
        if(1 != EVP_PKEY_derive(ctx, NULL, &secret_len)) handleErrors();

        /* Create the buffer */
        if(NULL == (secret = (unsigned char *)OPENSSL_malloc(secret_len))) handleErrors();

        /* Derive the shared secret */
        if(1 != (EVP_PKEY_derive(ctx, secret, &secret_len))) handleErrors();

        std::vector<unsigned char> retval( secret, secret + (ptrdiff_t)secret_len );

        OPENSSL_free(secret);

        return retval;
    }

    std::vector<unsigned char> derive_secret( const std::vector<unsigned char> & peerkey ) {
        // EVP_PKEY *d2i_PUBKEY(EVP_PKEY **a, const unsigned char **pp, long length);
        const unsigned char * ptr = peerkey.data();
        EVP_PKEY * key;
        if(NULL == (key = d2i_PUBKEY(NULL, &ptr, peerkey.size()))) handleErrors();

        auto retval = derive_secret(key);

        EVP_PKEY_free(key);

        return std::move(retval);
    }
    
private:
    ParameterGenerationCtx m_pctx;
    ParameterObjParams     m_params;
    KeyGenerationCtx       m_kctx;
    Privkey                m_pkey;
    SecretDerivationCtx    m_ctx;
};
