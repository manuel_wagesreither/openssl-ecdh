#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>

#define EXPOSE_INTERNALS
#include "ecdh.hpp"


template <class T>
std::string stringify( const T& data )
{
    std::string hex_tmp;
    for (auto byte : data) {
        std::ostringstream oss;
        oss << std::hex << std::setw(2) << std::setfill('0') << (unsigned)byte << " ";
        hex_tmp += oss.str();
    }

    // Delete final space character
    hex_tmp.pop_back();
    return hex_tmp;
}


int main(int, char**)
{
    // Test case 1
    {
        ecdh_key ecdh;
        EVP_PKEY * privkey = ecdh.get_privkey();
        auto pubkey = ecdh.get_pubkey();

        std::cout << "Pubkey: " << stringify( pubkey ) << std::endl;

        auto secret1 = ecdh.derive_secret( privkey );
        auto secret2 = ecdh.derive_secret( pubkey );
        auto secret3 = ecdh.derive_secret( privkey );
        auto secret4 = ecdh.derive_secret( pubkey );

        std::cout << "Secret: " << stringify( secret1 ) << std::endl;
        std::cout << "Secret: " << stringify( secret2 ) << std::endl;
        std::cout << "Secret: " << stringify( secret3 ) << std::endl;
        std::cout << "Secret: " << stringify( secret4 ) << std::endl;

        assert( secret1 == secret2 );
        assert( secret2 == secret3 );
        assert( secret3 == secret4 );
    }

    // Test case 2
    {
        ecdh_key key1, key2, key3;
        auto pubkey1 = key1.get_pubkey();
        auto pubkey2 = key2.get_pubkey();
        auto pubkey3 = key3.get_pubkey();

        std::cout << "Pubkey 1: " << stringify( pubkey1 ) << std::endl;
        std::cout << "Pubkey 2: " << stringify( pubkey2 ) << std::endl;
        std::cout << "Pubkey 3: " << stringify( pubkey3 ) << std::endl;

        assert( pubkey1 != pubkey2 );
        assert( pubkey2 != pubkey3 );
        assert( pubkey3 != pubkey1 );

        {
            auto secret12 = key1.derive_secret( pubkey2 );
            auto secret21 = key2.derive_secret( pubkey1 );

            std::cout << "1s shared secret with 2: " << stringify( secret12 ) << std::endl;
            std::cout << "2s shared secret with 1: " << stringify( secret21 ) << std::endl;

            assert( secret12 == secret21 );
        }
        {
            auto secret23 = key2.derive_secret( pubkey3 );
            auto secret32 = key3.derive_secret( pubkey2 );

            std::cout << "2s shared secret with 3: " << stringify( secret23 ) << std::endl;
            std::cout << "3s shared secret with 2: " << stringify( secret32 ) << std::endl;

            assert( secret23 == secret32 );
        }
        {
            auto secret31 = key3.derive_secret( pubkey1 );
            auto secret13 = key1.derive_secret( pubkey3 );

            std::cout << "3s shared secret with 1: " << stringify( secret31 ) << std::endl;
            std::cout << "1s shared secret with 3: " << stringify( secret13 ) << std::endl;

            assert( secret31 == secret13 );
        }
    }

    return 0;
}
